/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Modelo;

import java.util.Iterator;
import ufps.util.colecciones_seed.ListaS;

/**
 *
 * @author madar
 */
public class BigLong {

    private ListaS<Short> numeros = new ListaS();
    private boolean listainverida = false;

    public BigLong() {
    }

    /**
     * Ejemplo: numero = 125699 Lista =<1,2,5,6,9,9>
     *
     * @param numero una cadena con un número
     */
    public BigLong(String numero) throws Exception {
        if (numero.isEmpty()) {
            throw new RuntimeException("La cadena esta vacia");
        }
        char[] numero1 = numero.toCharArray();
        
        for (char comp : numero1) {

            if (comp >= 48 && comp <= 57) {
                numeros.insertarFin((short) Character.getNumericValue(comp));
            } else {
                throw new Exception("no es un numero");
            }
        }
    }
    
    
    private void base(BigLong num2) {
        ListaS listtemporal = this.numeros;
        this.numeros = num2.numeros;
        num2.numeros = listtemporal;
    }
    
    /**
     * Metodo para comprobar el mayor de dos listas
     * @param list listo que se va a comprar
     * @return un boolean
     */
    private boolean mayor(BigLong list) {
        boolean comprobar = false;

        if ((this.numeros.getTamanio() == list.numeros.getTamanio())) {
            short num1, num2;
            Iterator<Short> iterador1 = this.numeros.iterator();
            Iterator<Short> iterador2 = list.numeros.iterator();

            while (iterador1.hasNext()) {
                num1 = iterador1.next();
                num2 = 0;
                if (iterador2.hasNext()) {
                    num2 = iterador2.next();
                }
                if (num1 > num2) {
                    comprobar = true;
                    break;
                }
                if (num1 < num2) {
                    comprobar = false;
                    break;
                }

            }
        } else {
            comprobar = this.numeros.getTamanio() >= list.numeros.getTamanio();
        }
        return comprobar;
    }

    /**
     * metodo para eliminar los 0 de la lista resultado
     * @param result resultado de la lista
     */
    private void eliminar0(BigLong result) {
        short num;
        Iterator<Short> iterador = result.numeros.iterator();
        num = iterador.next();
        while (num == 0) {
            result.numeros.remove(0);
            num = iterador.next();
        }
    }

    /**
     * ****************************************
     * *********SOLO USANDO ITERADORES**********
     * ****************************************
     */
    
    
    
    
    /**
     * Metodo para sumar listas con iterador
     * @param num2 lista con la que se va a sumar
     * @return la nueva lista
     */
    public BigLong getSuma(BigLong num2) {
        if (num2.numeros.getTamanio() > this.numeros.getTamanio()) {
            this.base(num2);
        }

        if (!listainverida) {
            this.numeros.invertirLista();
            num2.numeros.invertirLista();
            listainverida = true;
        }

        BigLong listSum = new BigLong();
        short num1, nu2, co = 0, suma;
        Iterator<Short> iterador1 = this.numeros.iterator();
        Iterator<Short> iterador2 = num2.numeros.iterator();

        while (iterador1.hasNext()) {
            num1 = iterador1.next();
            nu2 = 0;
            
            if (iterador2.hasNext()) {
                nu2 = iterador2.next();
            }
            
            suma = (short) (num1 + nu2 + co);
            
            co = (short) (suma / 10);
            
            short res = (short) (suma % 10);
            listSum.numeros.insertarInicio(res);

            if (!iterador1.hasNext() && co > 0) {

                listSum.numeros.insertarInicio(co);
            }
        }
        return listSum;
    }
    
    /**
     * para restar listas con iterador
     * @param num2 lista con la que se va a restar
     * @return la nueva lista
     */
    public BigLong getResta(BigLong num2) {
        if (listainverida == true) {
            this.numeros.invertirLista();
            num2.numeros.invertirLista();
            listainverida = false;
        }

        if (this.mayor(num2) == false) {
            this.base(num2);

            this.numeros.invertirLista();
            num2.numeros.invertirLista();
        } else {
            this.numeros.invertirLista();
            num2.numeros.invertirLista();
        }

        BigLong listres = new BigLong();
        short num1, nu2, signum;
        Iterator<Short> iterador1 = this.numeros.iterator();
        Iterator<Short> iterador2 = num2.numeros.iterator();

        num1 = iterador1.next();
        while (iterador1.hasNext()) {

            nu2 = 0;

            if (iterador2.hasNext()) {
                nu2 = iterador2.next();
            }

            if (num1 < nu2) {
                signum = iterador1.next();
                signum--;
                num1 += 10;
                listres.numeros.insertarInicio((short) (num1 - nu2));
                num1 = signum;
            } else {
                listres.numeros.insertarInicio((short) (num1 - nu2));
                
                if (iterador1.hasNext()) {
                    num1 = iterador1.next();
                }
            }
            if (!iterador1.hasNext()) {
                
                if (num2.numeros.getTamanio() < this.numeros.getTamanio()) {
                    listres.numeros.insertarInicio((short) (num1));
                } else {
                    nu2 = iterador2.next();
                    
                    if (num1 - nu2 > 0) {
                        listres.numeros.insertarInicio((short) (num1 - nu2));
                    }
                }
            }
        }
        
        this.eliminar0(listres);
        return listres;
    }

    @Override
    public String toString() {
        return this.numeros.toString();
    }

}
