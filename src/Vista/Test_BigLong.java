/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Vista;

import Modelo.BigLong;

/**
 *
 * @author madar
 */
public class Test_BigLong {
    public static void main(String[] args) throws Exception {
        String num1="99999999999999999999999999999999";
        String num2="1";
        BigLong n1=new BigLong(num1);
        BigLong n2=new BigLong(num2);
        
        
        try {
            System.out.println("La suma de n1 con n2 es:"+n1.getSuma(n2).toString());
            //100000000000000000000000000000000
            System.out.println("La Resta de n1 con n2 es:"+n1.getResta(n2).toString());
            //99999999999999999999999999999998
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
        
    }
    
}
